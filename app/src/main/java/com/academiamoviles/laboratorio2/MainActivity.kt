package com.academiamoviles.laboratorio2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnCalcular.setOnClickListener {
            val anio= edtAnio.text.toString().toInt()
            when(anio){
                in 1930..1948 -> {tvResultado.text="Población: 6´300,000 Rasgo Caracteristico: Austeridad"}
                in 1949..1968 -> {tvResultado.text="Población: 12´000,000 Rasgo Caracteristico: Ambición"}
                in 1969..1980 -> {tvResultado.text="Población: 9´300,000 Rasgo Caracteristico: Obsesión por el éxito"}
                in 1981..1993 -> {tvResultado.text="Población: 7´200,000 Rasgo Caracteristico: Frustración"}
                in 1993..2010 -> {tvResultado.text="Población: 9´300,000 Rasgo Caracteristico: Irreverencia"}
            }
        }
    }
}